<?php

require 'Product.php';

$method = $_SERVER['REQUEST_METHOD'];
$datas = getDatas();
$toReturn = null;

//eraseDatas();die;

if($method == 'GET') {

	$toReturn = $datas;
	if (isset($_GET['type'])) {
		$toReturn = [];
		foreach ($datas as $product) {

			if ($product->type == $_GET['type']) {
				$toReturn[] = $product;
			}
		}
	}
	if (isset($_GET['product'])) {
		$toReturn = [];
		foreach ($datas as $product) {
			if ($product->id == $_GET['product']) {
				$toReturn = $product;
				break;
			}
		}
	}
} elseif ($method == 'POST') {
	$keys = ['name', 'description', 'price', 'type'];
	$error = null;
	$product = new Product();
	foreach($keys as $key){
		if(!array_key_exists($key, $_POST)){
			$error = ['error'=>'champ ' . $key . ' manquant'];
			break;
		}else {
			$method = 'set' . ucfirst($key);
			$product->$method($_POST[$key]);
		}
	}
	if(!$error){
		$id = $datas[count($datas) - 1]->id + 1;
		$product->setId($id);
		$datas[] = $product;
		saveDatas($datas);
		$toReturn = $product;
	} else {
		$toReturn = $error;
	}

} elseif($method == 'DELETE'){
	if(isset($_GET['id'])){
		foreach($datas as $key=>$product){
			if($product->id == $_GET['id']){
				unset($datas[$key]);
				saveDatas(array_values($datas));
				$toReturn = ['success'=>'produit '. $_GET['id'] . ' supprimé'];
				break;

			}
		}
	} else {
		$toReturn = ['error'=>'id du produit manquant'];
	}
}
echo json_encode($toReturn);
header('content-type:application/json');


/******************************************************************************/
//Fonctions
/******************************************************************************/

function getDatas()
{
	return json_decode(file_get_contents('./database.json'));
}

function saveDatas($datas)
{
	file_put_contents('./database.json', json_encode($datas));
}

function eraseDatas(){
	$product1 = new Product();
	$product1
		->setId(1)
		->setName('Sophie la giraffe')
		->setDescription('Fabriquée « artisanalement » depuis plus de 50 ans à base de caoutchouc 100% naturel')
		->setType(Product::TYPE_GAME)
		->setPrice(9);

	$product2 = new Product();
	$product2
		->setId(2)
		->setName('Bernard le castor')
		->setDescription('Fabriqué depuis plus de 10000 ans à base de pierres 100% naturelles')
		->setType(Product::TYPE_GAME)
		->setPrice(8);

	$product3 = new Product();
	$product3
		->setId(3)
		->setName('Chaise rose fluo')
		->setDescription('La chaise tendance')
		->setType(Product::TYPE_FURNISH)
		->setPrice(28);


	$product4 = new Product();
	$product4
		->setId(4)
		->setName('Chaise en peau de bernard le castor')
		->setDescription('La chaise confortable')
		->setType(Product::TYPE_FURNISH)
		->setPrice(45);

	$datas = [$product1, $product2, $product3, $product4];
	saveDatas($datas);die;
}