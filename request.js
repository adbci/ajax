function Request() {

    methods = {

        get: function (url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) /* 4 : état "complete" */
                {
                    if (xhr.status == 200) /* 200 : code HTTP pour OK */
                    {
                        //Traitement de la réponse.
                        callback(JSON.parse(xhr.responseText));
                    }
                }
            };

            xhr.open("GET", url);
            xhr.send();
        },
        post: function (url, data, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        callback(JSON.parse(xhr.responseText));
                    }
                }
            };
            xhr.open("POST", url);

            //envoi des infos
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            // va en second parametre
            // xhr.send("name=" + name.value + "&type=" + type.value + "&description=" + description.value + "&price=" + price.value);
            xhr.send(data);
        },
        delete: function (url, callback) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) /* 4 : état "complete" */
                {
                    if (xhr.status == 200) /* 200 : code HTTP pour OK */
                    {
                        //Traitement de la réponse.
                        callback(JSON.parse(xhr.responseText));
                    }
                }
            };

            xhr.open("DELETE", url);
            xhr.send();
        }

    };

    return methods;
}